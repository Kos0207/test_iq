// Instantiating the global app object
var app = {};

document
  .querySelector(".navbar__hamburger-lines")
  .addEventListener("click", function (e) {
    document.querySelector(".navbar__hamburger-lines").classList.add("active");
    document.querySelector(".menu").classList.add("active");
  });

document
  .querySelector(".menu__close-img")
  .addEventListener("click", function (e) {
    document.querySelector(".menu").classList.remove("active");
    document
      .querySelector(".navbar__hamburger-lines")
      .classList.remove("active");
  });
//вибір квадрата та одночасна активація кнопки "Next"
let squares = document.querySelectorAll(".square,.answer-options-item");
let btnNext = document.querySelector(".btn-silver");
squares.forEach(function (square) {
  square.addEventListener("click", function () {
    squares.forEach(function (square) {
      square.classList.remove("active");
    });
    square.classList.add("active");
  });
});

//start
let btnsStart = document.querySelectorAll(".js-btn-start");
btnsStart.forEach((btnStart) => {
  btnStart.addEventListener("click", function (e) {
    e.preventDefault();
    document.body.classList.remove("is-page-1");
    document.body.classList.add("is-page-2");
    document
      .querySelector(".footer__content-copyright")
      .classList.add("hidden");
    document
      .querySelector(".footer__content-copyright-img")
      .classList.add("hidden");
  });
});

//steps
document.querySelector(".js-step-next").addEventListener("click", function (e) {
  e.preventDefault();
  const activeStep = document.querySelector(".step.active");
  const activeIndex = parseInt(activeStep.getAttribute("data-step"));
  const totalSteps = document.querySelectorAll(".step").length;
  if (activeIndex < totalSteps) {
    activeStep.classList.remove("active");
    const nextIndex = activeIndex + 1;
    document
      .querySelector(`.step[data-step="${nextIndex}"]`)
      .classList.add("active");
    const progresBar = document.querySelector(".progress__value");
    progresBar.style.width = (nextIndex / totalSteps) * 100 + "%";
  }
  //preloading+btn hidden
  if (activeIndex == totalSteps - 1) {
    document.querySelector(".preloader").classList.add("loaded");
    document.querySelector(".btn-silver").classList.add("hidden");
    setTimeout((activeStep) => {
      document.querySelector(".preloader").classList.remove("loaded");
      document.querySelector(".steps__form").classList.add("hidden");
      document.querySelector(".steps__result").classList.add("active");
      document.querySelector(".navbar__logo-title").textContent = "ГОТОВО!";
    }, 2000);
  }
});

//timer-countdown
function getTimeRemaining(endtime) {
  let time = Date.parse(endtime) - Date.parse(new Date());
  const seconds = Math.floor((time / 1000) % 60);
  const minutes = Math.floor((time / 1000 / 60) % 60);
  return {
    total: time,
    minutes: minutes,
    seconds: seconds,
  };
}
function initializeClock(id, endtime) {
  let clock = document.getElementById(id);
  let minutesSpan = clock.querySelector(".minutes");
  let secondsSpan = clock.querySelector(".seconds");

  function updateClock() {
    let time = getTimeRemaining(endtime);
    minutesSpan.innerHTML = ("0" + time.minutes).slice(-2);
    secondsSpan.innerHTML = ("0" + time.seconds).slice(-2);
    if (time.total === 0) {
      clearInterval(timeinterval);
    }
  }
  updateClock();
  const timeinterval = setInterval(updateClock, 1000);
}
let deadline = new Date(Date.parse(new Date()) + 10 * 60 * 1000); // for endless timer
initializeClock("countdown", deadline);

//api
let url = "https://swapi.dev/api/people/1/";
document.querySelector(".btn-call-red").addEventListener("click", function (e) {
  e.preventDefault();
  function getDatas() {
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw new Error("url addres is not working");
        }
        return response.json();
      })
      .then((el) => {
        let datasList = document.getElementById("datas-list");
        console.log(el);
        let res = document.createElement("li");
        res.innerHTML = `<p><strong>name:</strong> ${el.name} <br>
          <strong>gender:</strong> ${el.gender} <br>
          <strong>mass:</strong> ${el.mass} <br>
          <strong>height:</strong> ${el.height}
          </p>`;
        datasList.appendChild(res);
      })
      .catch((error) => console.log(error));
  }
  getDatas();
});
